# Set Visual Studio Variables
pushd 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build'
cmd /c "vcvarsall.bat x86 & set" |
foreach {
  if ($_ -match "=") {
    $v = $_.split("="); set-item -force -path "ENV:\$($v[0])"  -value "$($v[1])"
  }
}
popd

$toppath = Get-Location
$qtpath = "$toppath\qtbuild"

git clone git://code.qt.io/qt/qt5.git
cd qt5
git checkout 5.15.2

$qtsrcpath = Get-Location

$env:Path = "$qtsrcpath\qtbase\bin;$env:Path"
$env:Path = "$qtsrcpath\gnuwin32\bin;$env:Path"
$env:Path = "$qtsrcpath\qtrepotools\bin;$env:Path"

perl init-repository --module-subset=qtbase,qtrepotools,qttools
.\configure.bat -release -nomake examples -nomake tests -opensource -confirm-license -prefix $qtpath
jom -j4
jom install

cd $qtpath
7z a -r ..\qt.zip *
cd $toppath